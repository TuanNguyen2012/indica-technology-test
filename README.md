# Welcome

Assignment for the position of React Native Developer

Platforms:
- iOS
- Android

## Tech
- React Native 0.64.x (https://reactnative.dev/docs/getting-started)
- Typescript (https://www.typescriptlang.org)
- React Hook (https://reactjs.org/docs/hooks-intro.html)
- React Redux Toolkit (https://redux-toolkit.js.org/)
- React Native Gifted Chat (https://github.com/FaridSafi/react-native-gifted-chat)
- Firebase (https://cloud.google.com/firestore/docs/client/get-firebase)
- Responsive UI react-native-size-matters (https://github.com/nirsky/react-native-size-matters)
- Axios (https://github.com/axios/axios)
- Eslint (https://eslint.org)
- React Native Component Libraries (yarn add 'package-name')
....
## Gitlab
Gitlab (https://docs.gitlab.com/ee/intro/)

### Install Node Package

#### Option A: yarn (recommended)
```shell
yarn install
```

#### Option B: npm
```shell
npm install
```

### iOS
#### Install with CocoaPods
Install Pods with

```shell
pod install
```

### Android
Android automatically configures the library

#### Run
### IOS
```shell
react-native run-ios
```

### Android
```shell
react-native run-android
```

## Coding convention
### Kiểm tra Coding convention
```shell
yarn run lint
```

### Fix bugs
```shell
yarn run lint --fix
```

### Performance Monitoring
- https://rnfirebase.io/perf/usage

## Contact
- Tuan (Jason) Nguyen:
    + mail: <nguyentuanit97@gmail.com>
    + phone: 0355201297
