
declare module "toggle-switch-react-native";
declare module "react-native-photo-view";
declare module "lodash";
declare module "react-native-date-picker";
declare module "react-native-controlled-mentions";
declare module "react-native-easy-toast";
declare module 'react-native-thumbnail-grid';
declare module "react-native-keyboard-spacer";
declare module "react-native-modal-dropdown";
declare module "react-native-svg-charts";
declare module "socket.io-client";
declare module "react-native-progress-wheel";
declare module "react-native-charts-wrapper";
declare module "react-native-version-check";
declare module "react-native-qrcode-scanner-view";