const icons = {
  tabbar: {
    home: require('@/assets/icons/Tabbar/home.png'),
    menu: require('@/assets/icons/Tabbar/menu.png'),
  },
  login: {
    facebook: require('@/assets/icons/Login/facebook.png'),
    google: require('@/assets/icons/Login/google.png'),
  },
  home: {
    comment: require('@/assets/icons/Home/comment.jpeg'),
  }
};

export default icons;
