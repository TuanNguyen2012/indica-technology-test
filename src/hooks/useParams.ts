import {useRoute} from '@react-navigation/core';

export const useParams = () => {
  const params = useRoute().params || ({} as any);
  return params;
};

export default useParams;
