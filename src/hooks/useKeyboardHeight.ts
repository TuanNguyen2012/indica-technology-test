import {useEffect, useRef, useState} from 'react';
import {Keyboard, KeyboardEvent, Platform, StatusBar} from 'react-native';

export const useGetKeyboardHeight = () => {
  const heightRef = useRef(300);
  const statusBarHeight = StatusBar.currentHeight || 0;
  const [heightState, setHeightState] = useState(0);

  const onKeyboardShow = (keyboard: KeyboardEvent) => {
    const height =
      (keyboard?.endCoordinates?.height || heightRef.current) + statusBarHeight;
    heightRef.current = height;
    setHeightState(height);
  };

  const onKeyboardHide = () => {
    setHeightState(0);
  };

  useEffect(() => {
    const ios = Platform.OS === 'ios';
    const showSub = Keyboard.addListener(
      ios ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyboardShow,
    );
    const hideSub = Keyboard.addListener(
      ios ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyboardHide,
    );

    return () => {
      showSub.remove();
      hideSub.remove();
    };
  }, []);

  return {heightRef: heightRef.current, heightState};
};

export default useGetKeyboardHeight;
