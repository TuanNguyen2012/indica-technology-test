import { initializeApp } from 'firebase/app';
import { initializeFirestore } from 'firebase/firestore';
import { Settings } from 'react-native-fbsdk-next';

export const setup = () => {
  // Setting Facebook
  Settings.initializeSDK();
  Settings.setAppID('711504947082408');

  // Setting Firebase
  const firebaseConfig = {
    apiKey: "AIzaSyBR4K0rtZOAGW_5Sn85NswKK0C7-SN-G-s",
    authDomain: "indica-technology-test.firebaseapp.com",
    projectId: "indica-technology-test",
    storageBucket: "indica-technology-test.appspot.com",
    messagingSenderId: "607594023340",
    appId: "1:607594023340:web:b4066078154be5d8429209"
  }

  const app = initializeApp(firebaseConfig);

  // initializeApp(apiKeys.firebaseConfig);

  initializeFirestore(app, {
    experimentalForceLongPolling: true,
  });
};