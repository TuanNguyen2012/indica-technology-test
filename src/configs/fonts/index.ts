import { moderateScale } from 'react-native-size-matters';

export const font = {

};

export const font_size = {
  INPUT_KEYBOARD: moderateScale(17, 0.1),
  VERY_LARGE_x3: moderateScale(24, 0.1),
  VERY_LARGE_x2: moderateScale(18, 0.1),
  VERY_LARGE: moderateScale(16, 0.1),
  LARGE: moderateScale(15, 0.1),
  NORMAL: moderateScale(14, 0.1),
  SMALL: moderateScale(13, 0.1),
  VERY_SMALL: moderateScale(12, 0.1),
  VERY_SMALL_x1_5: moderateScale(11, 0.1),
  VERY_SMALL_x2: moderateScale(10, 0.1),
  VERY_SMALL_x3: moderateScale(8, 0.1),
  ICONS: moderateScale(24, 0.1),
};

export const size = (px: number) => {
  return moderateScale(px, 0.1);
};

export const color = {
  avatar: '#e7e9eb'
}
