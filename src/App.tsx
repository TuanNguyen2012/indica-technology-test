import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import initI18n from './i18n/config';
initI18n();

import { setup } from '@/configs/setup';
setup();

import { QueryClient, QueryClientProvider } from "react-query";
const queryClient = new QueryClient();

import { Routes } from './routes';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '@/redux';
import { setStore } from '@/redux/getStore';

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
setStore(store);

function App() {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <Routes />
        </QueryClientProvider>
      </Provider>
    </SafeAreaProvider>
  );
};

export default App;
