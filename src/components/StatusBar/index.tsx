import { StatusBarProps, StatusBar } from 'react-native';
import React, { memo } from 'react';
interface Props extends Partial<StatusBarProps> {
  delay?: boolean;
  theme?: 'light-content' | 'dark-content' | 'default';
  backgroundColor?: string
}

export const StatusBarView = memo(({ delay, theme, backgroundColor, ...props }: Props) => {
  return (
    <>
      <StatusBar barStyle={theme} {...props} backgroundColor={backgroundColor ? backgroundColor : 'white'} />
    </>
  );
});
