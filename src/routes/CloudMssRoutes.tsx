import OneSignal from 'react-native-onesignal';

// Method for handling notifications received while app in foreground
OneSignal.setNotificationWillShowInForegroundHandler((notificationReceivedEvent: any) => {
    let notification = notificationReceivedEvent.getNotification();
    notificationReceivedEvent.complete(notification);
});

// Method for handling notifications opened
OneSignal.setNotificationOpenedHandler((notification: any) => {
    const payload = notification?.notification?.additionalData;
    if (payload) {
    }
});
