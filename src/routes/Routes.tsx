import React, { memo, useRef, useCallback } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from '@/utils/navigation';

const RootStack = createStackNavigator();
const AuthStack = createStackNavigator();
const MainStack = createStackNavigator();

const AuthStackComponent = memo(function AuthStackComponent() {
  return (
    <AuthStack.Navigator initialRouteName={'Splash'} headerMode={'none'}>
      <AuthStack.Screen name={'Splash'} component={require('../features/AuthStack/Splash').default} />
      <AuthStack.Screen name={'Login'} component={require('../features/AuthStack/Login').default} />
    </AuthStack.Navigator>
  );
});

const MainStackComponent = memo(function MainStackComponent() {
  return (
    <>
      <MainStack.Navigator initialRouteName={'TabNavigator'} headerMode={'none'}>
        <MainStack.Screen name={'TabNavigator'} component={require('../features/MainStack/TabNavigator').default} />
        <MainStack.Screen name={'Comment'} component={require('../features/MainStack/Comment').default} />
      </MainStack.Navigator>
    </>
  );
});

export const Routes = memo(function Routes() {
  const routeNameRef = useRef('');
  const onStateChange = useCallback(() => {
    const previousRouteName = routeNameRef.current;
    const currentRouteName = navigationRef.current?.getCurrentRoute()?.name;

    if (currentRouteName && previousRouteName !== currentRouteName) {
      routeNameRef.current = currentRouteName;
    }
  }, []);
  return (
    <>
      <NavigationContainer
        ref={navigationRef}
        onStateChange={onStateChange}>
        <RootStack.Navigator initialRouteName="AuthStack" headerMode={'none'}>
          <RootStack.Screen name={'AuthStack'} component={AuthStackComponent} />
          <RootStack.Screen name={'MainStack'} component={MainStackComponent} />
        </RootStack.Navigator>
      </NavigationContainer>
    </>
  )
});
