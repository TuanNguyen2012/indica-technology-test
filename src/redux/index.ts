import { combineReducers } from 'redux';
import userSlice from './UserSlice';
const rootReducer = combineReducers({
    userSlice: userSlice,
});
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
