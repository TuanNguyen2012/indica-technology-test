import { createSlice } from '@reduxjs/toolkit';

export const initialUserState = {
   data: {
    email: '',
    firstName: '',
    phone: ''
   }
};

const userSlice = createSlice({
    name: 'user',
    initialState: initialUserState,
    reducers: {
        setUser(state, action) {
            state.data = action.payload;
        },
    }
});

export const { setUser } = userSlice.actions;
export default userSlice.reducer;