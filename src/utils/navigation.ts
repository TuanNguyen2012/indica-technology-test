import React from 'react';
import { NavigationContainerRef, StackActions, CommonActions } from '@react-navigation/native';

export const navigationRef = React.createRef<NavigationContainerRef<any>>();

export const navigation = () => navigationRef.current!;

export const navigate = (screenName: string, params?: object) => navigation().navigate(screenName, params);

export const replace = (screenName: string, params?: object) => navigation().dispatch(StackActions.replace(screenName, params));

export const push = (screenName: string, params?: object) => navigation().dispatch(StackActions.push(screenName, params));

export const reset = (screenName: string) => () => (
  navigation().dispatch(CommonActions.reset({
    index: 1,
    routes: [
      { name: screenName }
    ]
  }))
);

export const goBack = () => navigation().goBack();

export const popToTop = () => {
  navigation().canGoBack() && navigation().dispatch(StackActions.popToTop());
};