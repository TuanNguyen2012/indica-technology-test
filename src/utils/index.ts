import { Alert, Dimensions } from "react-native";
const { height, width } = Dimensions.get('window');
export const height_screen = width < height ? height : width;
export const width_screen = width < height ? width : height;
