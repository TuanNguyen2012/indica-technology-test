export const login = {
    "login": {
        "forgotPassword": "Forgot your password?",
        "loginText": "SIGN IN",
        "noAccount": "Don't have an account yet? ",
        "signUp": "SIGN UP"
    },
}