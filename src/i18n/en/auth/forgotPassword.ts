export const forgotPassword = {
    "forgotPassword": {
        "resetPassword": "Reset password",
        "checkMail": "Check your mail",
        "content": "Enter your email address, we will send you an OTP for the next steps",
        "emailAddress": "Email Address",
        "sendOTP": "Send OTP code",
        "sentOTP": "We have sent the OTP to your email address, please check and fill it in here",
        "OTPInput": "Enter OTP",
        "continue": "Tiếp tục",
        "resendOTP": "Resend verification code",
        "wrongOTP": "Wrong OTP verification code",
        "newPassword": "Create new password",
        "updatePassword": "Update your new password for system login",
        "password": "Password",
        "require1": "Minimum of 8 characters",
        "require2": "The two passwords must match",
        "confirm": "Confirm password"
    },
}