import { tabbar } from "./taskbar";
import { login, forgotPassword } from "./auth";

export const EN = {
    ...global,
    ...tabbar,
    ...login,
    ...forgotPassword,
}