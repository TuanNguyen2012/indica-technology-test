export const forgotPassword = {
    "forgotPassword": {
        "resetPassword": "Quên mật khẩu",
        "checkMail": "Kiểm tra mail",
        "content": "Nhập địa chỉ email của bạn, chúng tôi sẽ gửi cho bạn mã OTP để thực hiện các bước tiếp theo",
        "emailAddress": "Địa chỉ email",
        "sendOTP": "Gửi mã OTP",
        "sentOTP": "Chúng tôi đã gửi mã OTP về địa chỉ email cho bạn, vui lòng kiểm tra và điền vào đây",
        "OTPInput": "Nhập mã OTP",
        "continue": "Tiếp tục",
        "resendOTP": "Gửi lại mã xác thực",
        "wrongOTP": "Sai mã xác thực OTP",
        "newPassword": "Tạo mật khẩu mới",
        "updatePassword": "Cập nhật mật khẩu mới của bạn cho việc đăng nhập hệ thống",
        "password": "Mật khẩu",
        "require1": "Tối thiểu 8 ký tự",
        "require2": "Hai mật khẩu phải trùng nhau",
        "confirm": "Xác nhận mật khẩu"
    },
}