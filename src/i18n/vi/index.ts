import { tabbar } from "./tabbar";
import { login, forgotPassword } from "./auth";

export const VN = {
    ...global,
    ...tabbar,
    ...login,
    ...forgotPassword,
}