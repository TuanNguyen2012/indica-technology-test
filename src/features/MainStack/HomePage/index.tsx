import icons from '@/assets/icons';
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, StyleSheet, Image, Text, View, FlatList, SafeAreaView } from 'react-native';
import { font, size } from '@/configs/fonts';
import { useHomePage } from './useHomePage';
import FastImage from 'react-native-fast-image';
import { width_screen } from '@/utils';
import { navigate } from '@/utils/navigation';

const HomePage = () => {
    const { dummyPost } = useHomePage();
    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={dummyPost}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (<ItemPost item={item} />)}
            />
        </SafeAreaView>
    );
};

interface itemProps {
    postId: string;
    author: {
        id: number;
        name: string;
    },
    content: string;
    image: string;
}

const ItemPost = ({ item }: {
    item: itemProps
}) => {
    return (
        <View style={styles.viewContainerItem}>
            <Text>{item.content}</Text>
            <FastImage
                source={{
                    uri: item.image,
                }}
                style={styles.images}
            />
            <Text style={styles.txtAuthorName}>{item.author.name}</Text>

            <TouchableOpacity style={styles.viewComment} onPress={() => navigate('Comment', { postId: item.postId })}>
                <Image
                    source={icons.home.comment}
                    style={styles.icoComment}
                />
                <Text>  Comment this thead {'>>'}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default HomePage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    images: {
        width: width_screen,
        height: size(300),
        marginTop: size(15)
    },
    txtAuthorName: {
        marginTop: size(10),
        fontWeight: 'bold'
    },
    viewContainerItem: {
        marginBottom: size(10),
        borderBottomColor: '#CCCCCC',
        borderBottomWidth: size(8),
        paddingBottom: size(10)
    },
    viewComment: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#CCCCCC',
        margin: size(10),
        borderTopRightRadius: size(10),
        borderBottomRightRadius: size(10)
    },
    icoComment: {
        width: size(36),
        height: size(36)
    }
});