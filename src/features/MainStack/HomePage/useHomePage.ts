
export const useHomePage = () => {
    const dummyPost = [
        {
            postId: '1',
            author: {
                id: 1,
                name: 'Messi'
            },
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent fermentum mi mi, at fringilla enim fermentum in. Fusce vestibulum at nisl non pharetra. Cras lacinia dapibus elit, id malesuada tellus vulputate at. Integer vestibulum viverra tellus, vitae volutpat orci dignissim sagittis. Quisque varius turpis metus, eget condimentum dui faucibus eget.',
            image: 'https://i-thethao.vnecdn.net/2022/12/19/messi-10-1671398727-8892-1671398797.jpg'
        },
        {
            postId: '2',
            author: {
                id: 2,
                name: 'Indica Technology'
            },
            content: 'Thank you again for applying for the Front-End Developer position. As a part of our selection process, we send assignments to selected candidates, and you are one of them!',
            image: 'https://thumbs.dreamstime.com/b/beautiful-landscape-dry-tree-branch-sun-flowers-field-against-colorful-evening-dusky-sky-use-as-natural-background-backdrop-48319427.jpg'
        },
        {
            postId: '3',
            author: {
                id: 3,
                name: 'Jason Nguyen'
            },
            content: 'Thank you again for applying for the Front-End Developer position. As a part of our selection process, we send assignments to selected candidates, and you are one of them!',
            image: 'https://images.pexels.com/photos/794494/pexels-photo-794494.jpeg?cs=srgb&dl=pexels-anthony-%29-794494.jpg&fm=jpg'
        }
    ]
    return {
        dummyPost
    }
}