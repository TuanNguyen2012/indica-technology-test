
import { Image, StyleSheet } from 'react-native';
import React, { memo } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import icons from '@/assets/icons';
const Tab = createBottomTabNavigator();
import { font, font_size } from '@/configs/fonts';
import { useTranslation } from 'react-i18next';
import { size } from '@/configs/fonts';
import '@/routes/CloudMssRoutes';

const TabNavigator = memo(function () {
    const { t } = useTranslation();
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: 'rgb(0, 96, 225)',
                inactiveTintColor: '#8C8C8C',
                keyboardHidesTabBar: true,
                labelPosition: 'below-icon',
                labelStyle: styles.labelStyleMobile
            }}
            initialRouteName={'HomePage'}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    let iconName;
                    switch (route.name) {
                        case 'HomePage':
                            iconName = icons.tabbar.home;
                            break;
                        case 'Account':
                            iconName = icons.tabbar.menu;
                            break;
                    }
                    return (
                        <Image source={iconName}
                            style={[styles.iconTab, { tintColor: focused ? 'rgba(0, 96, 225, 0.9)' : '#8C8C8C' }]}
                        />
                    );
                },
            })}
        >
            <Tab.Screen
                name='HomePage'
                component={require('../HomePage').default}
                options={{
                    tabBarLabel: t('tabbar.homepage')
                }}
            />
            <Tab.Screen
                name='Account'
                component={require('../Account').default}
                options={{
                    tabBarLabel: t('tabbar.account')
                }}
            />
        </Tab.Navigator>
    );
})

export default TabNavigator;

const styles = StyleSheet.create({
    labelStyleMobile: {
        fontSize: size(10)
    },
    iconTab: {
        width: font_size.ICONS,
        height: font_size.ICONS,
    }
});
