import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { size } from '@/configs/fonts';
import { useNavigation } from '@react-navigation/core';
import { useDispatch } from 'react-redux';
import Storage, { Users } from '@/utils/Storage';
import { initialUserState, setUser } from '@/redux/UserSlice';
import { StackActions } from '@react-navigation/native';

const Account = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch(); 
    const onLogut = async () => {
        await Storage.removeData(Users);
        dispatch(setUser(initialUserState));
        navigation.dispatch(
            StackActions.replace('AuthStack')
        );
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.btnLogout} onPress={onLogut}>
                <Text style={styles.txtLogout}>Logout</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Account;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    btnLogout: {
        backgroundColor: 'blue',
        padding: size(20),
        borderRadius: size(15)
    },
    txtLogout: {
        color: 'white'
    }
});