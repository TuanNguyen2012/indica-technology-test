import { useState, useEffect } from 'react';
import { getFirestore, addDoc, collection, onSnapshot } from 'firebase/firestore';
import _ from 'lodash';
export interface ItemCommentProps {
    postId: string;
    comment: string;
    author: string;
    time: number;
}

export const useComment = (postId: string) => {
    const firestore = getFirestore();
    const [listMessages, setListMessages] = useState<ItemCommentProps[]>([]);

    useEffect(() => {
        const unsub = onSnapshot(collection(firestore, "Post", postId, 'comment'), (querySnapshot) => {
            let listComments: ItemCommentProps[] = [];
            querySnapshot.docs.map((doc) => {
                let newData: any = {
                    ...doc.data(),
                }
                listComments.push(newData)
            });

            listComments = _.sortBy(listComments, ['time']);
            setListMessages(listComments);
        });
        return () => unsub();
    }, []);

    const onSendMessageToFirebase = async (payload: ItemCommentProps) => {
        try {
            let returnStatus = false;
            await addDoc(collection(firestore, "Post", payload.postId, "comment"), payload)
                .then(() => {
                    returnStatus = true;
                })
            return returnStatus;
        } catch (error) {
            console.log(error)
            return false;
        }
    }

    return {
        onSendMessageToFirebase,
        listMessages
    }
}