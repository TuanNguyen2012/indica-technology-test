import React, { useState } from 'react';
import { Platform, TextInput, FlatList, StyleSheet, KeyboardAvoidingView, Text, View, TouchableOpacity, SafeAreaView } from 'react-native';
import { font_size, size } from '@/configs/fonts';
import moment from 'moment';
import { goBack } from '@/utils/navigation';
import { ItemCommentProps, useComment } from './useComment';
import { useAppSelector, useParams } from '@/hooks';

const Comment = () => {
    const params = useParams();
    const firstName = useAppSelector(state => state.userSlice.data.firstName);
    const [message, setMessage] = useState('');
    const { onSendMessageToFirebase, listMessages } = useComment(params?.postId);

    const onSend = () => {
        onSendMessageToFirebase({
            postId: params?.postId,
            comment: message,
            author: firstName,
            time: new Date().getTime()
        }).then(() => {
            setMessage('');
        });
    }

    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? 'padding' : 'height'} keyboardVerticalOffset={0}>
                <TouchableOpacity style={styles.btnBack} onPress={() => goBack()}>
                    <Text>{'<<'} Back</Text>
                </TouchableOpacity>

                <FlatList
                    data={listMessages}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (<ItemComment item={item} />)}
                />

                <View style={styles.viewInput}>
                    <TextInput
                        placeholder='Typing ....'
                        style={styles.textInput}
                        value={message}
                        onChangeText={(text: string) => setMessage(text)}
                    />
                    <TouchableOpacity style={styles.btnSend} onPress={onSend}>
                        <Text style={{ color: 'white' }}>Send</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
};



const ItemComment = ({ item }: { item: ItemCommentProps }) => {
    return (
        <View style={styles.viewContainerItem}>
            <View style={styles.viewAuthor}>
                <Text>{item.author}</Text>
                <Text style={styles.txtContent}>{item.comment}</Text>
            </View>
            <Text style={styles.txtTime}>{moment(item.time).format('DD/MM/YYYY HH:mm')}</Text>
        </View>
    )
}

export default Comment;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    btnBack: {
        padding: size(15)
    },
    viewContainerItem: {
        margin: size(15),
        marginBottom: 0,
    },
    viewAuthor: {
        backgroundColor: '#CCCCCC',
        alignSelf: 'flex-start',
        paddingVertical: size(5),
        paddingHorizontal: size(15),
        borderRadius: size(8),
    },
    txtContent: {
        fontWeight: '700'
    },
    txtTime: {
        fontSize: font_size.VERY_SMALL,
        color: 'gray',
        marginTop: size(3)
    },
    viewInput: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: size(15),
    },
    textInput: {
        padding: size(15),
        borderRadius: size(10),
        borderWidth: 1,
        borderColor: '#CCCCCC',
        marginRight: size(10),
        flex: 1
    },
    btnSend: {
        backgroundColor: 'blue',
        padding: size(15),
        borderRadius: size(10),
    }
});