import { useState, useEffect } from 'react';
import { LoginManager, AccessToken } from "react-native-fbsdk-next";
import { getAuth, signInWithCredential } from 'firebase/auth';
import { getFirestore, setDoc, doc, getDoc } from 'firebase/firestore';
import { FacebookAuthProvider, GoogleAuthProvider } from "firebase/auth";
import { GoogleSignin, statusCodes } from "@react-native-google-signin/google-signin";
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/core';
import { setUser } from '@/redux/UserSlice';
import { StackActions } from '@react-navigation/native';
import Storage, { Users } from '@/utils/Storage';

export const useSignin = () => {
    const [loading, setLoading] = useState(false);
    const [showError, setShowError] = useState(false);
    const dispatch = useDispatch();
    const navigation = useNavigation();

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ["email"], // what API you want to access on behalf of the user, default is email and profile
            webClientId:
                "607594023340-uei50mu6r7mh3r02vt6176dl53scvll6.apps.googleusercontent.com",
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        });
    }, []);

    const handleFacebookSignin = async () => {
        setLoading(true);
        try {
            const result = await LoginManager.logInWithPermissions([
                "public_profile",
                "email",
            ]);

            if (result.isCancelled) {
                throw "User cancelled the login process";
            }

            const data = await AccessToken.getCurrentAccessToken();
            if (!data) {
                setShowError(true);
            } else {
                const credential = FacebookAuthProvider.credential(data.accessToken);
                const auth = getAuth();
                const userData: any = await signInWithCredential(auth, credential);
                if (userData) {
                    OnUserAccessWithCredential(
                        userData?.user?.uid,
                        userData?.user?.email,
                        userData?.user?.displayName,
                        userData?.user?.phoneNumber,
                        dispatch
                    ).then((status: boolean) => {
                        setLoading(false);
                        console.log("status", status);
                        if (status) {
                            navigation.dispatch(
                                StackActions.replace('MainStack')
                            );
                        } else {
                            setShowError(true);
                        }
                    });
                }
            }
            setLoading(false);
        } catch (error: any) {
            setLoading(false);
            console.log(error, "error");
        }
    };

    const googleSignOut = async () => {
        try {
            await GoogleSignin.signOut();
        } catch (error) {
            console.error(error);
        }
    };

    const handleGoogleSignin = async () => {
        setLoading(true);
        try {
            await GoogleSignin.hasPlayServices();
            const { idToken } = await GoogleSignin.signIn();
            const credential = GoogleAuthProvider.credential(idToken);
            const auth = getAuth();
            const userData: any = await signInWithCredential(auth, credential);
            if (userData) {
                OnUserAccessWithCredential(
                    userData?.user?.uid,
                    userData?.user?.email,
                    userData?.user?.displayName,
                    userData?.user?.phoneNumber,
                    dispatch
                ).then((status: boolean) => {
                    googleSignOut();
                    setLoading(false);
                    navigation.dispatch(
                        StackActions.replace('MainStack')
                    );
                    if (status) {
                    } else {
                        setShowError(true);
                    }
                });
            }
            setLoading(false);
        } catch (error: any) {
            setLoading(false);
            console.log(error, "error");
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    return {
        loading,
        showError,
        handleFacebookSignin,
        handleGoogleSignin
    }
}

export const OnUserAccessWithCredential = async (userId: string, email: string, firstName: string, phoneNumber: string, dispatch: any) => {
    try {
        const firestore = getFirestore();
        let returnStatus = false;
        let userDetail: any = await getDoc(doc(firestore, "Users", userId));
        if (userDetail.data()) { // It's mean have account, continues handle sign in
            const curUser = userDetail.data();
            // Handle save on redux 
            dispatch(setUser(curUser));
            Storage.setData(Users, curUser);
            return true;
        }

        // It's mean don't have account, continues handle sign up
        const curUser = {
            firstName: firstName,
            email: email,
            phone: phoneNumber,
        }
        await setDoc(doc(firestore, "Users", userId), curUser)
            .then(() => {
                returnStatus = true;
                dispatch(setUser(curUser));
                Storage.setData(Users, curUser);
            })
        return returnStatus;
    } catch (error) {
        console.log(error)
        return false;
    }
}