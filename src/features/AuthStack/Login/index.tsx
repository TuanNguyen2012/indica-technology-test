import React, { memo, useState } from 'react';
import { StatusBar, Text, SafeAreaView, View, FlatList, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { font, font_size, size } from '@/configs/fonts';
import icons from '@/assets/icons';
import { goBack, navigate } from '@/utils/navigation';
import LinearGradient from 'react-native-linear-gradient';
import { useSignin } from './useLogin';

const Login = memo(() => {
  const { loading, showError, handleFacebookSignin, handleGoogleSignin } = useSignin();
  return (
    <>
      <LinearGradient colors={['#0b4a4e', '#1b3e53', '#363153', '#4b3453']} style={styles.container}>
        <StatusBar barStyle={'light-content'} />
        <View style={styles.content}>
          {showError ? <Text>Something wrong!!!!</Text> : <></>
          }
          <Text style={styles.txtTitle}>Welcome Login Page</Text>
          <TouchableOpacity onPress={handleFacebookSignin}>
            <Image
              source={icons.login.facebook}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={handleGoogleSignin}>
            <Image
              source={icons.login.google}
            />
          </TouchableOpacity>
        </View>
      </LinearGradient>
    </>
  );
});


const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  txtTitle: {
    color: 'white',
    fontSize: font_size.VERY_LARGE,
    marginBottom: size(30)
  }
});

export default Login;
