import React, { useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Storage, { Token, Language, Users } from '@/utils/Storage';
import { useNavigation } from '@react-navigation/core';
import { StackActions } from '@react-navigation/native';
import i18next from 'i18next';
import { font, font_size } from '@/configs/fonts';
import { useDispatch } from 'react-redux';
import { setUser } from '@/redux/UserSlice';

const SplashScreen = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    useEffect(() => {
        const initApp = async () => {
            const language = await Storage.getData(Language);
            const curUser = await Storage.getData(Users);
            if (language == 'en') {
                i18next.changeLanguage('en');
            }
            setTimeout(async () => {
                if (curUser) {
                    dispatch(setUser(curUser));
                    navigation.dispatch(
                        StackActions.replace('MainStack')
                    );
                } else {
                    navigation.dispatch(
                        StackActions.replace('Login')
                    );
                }
            }, 1000);
        }
        initApp();
    }, []);

    return (
        <>
            <View style={styles.container}>
                <Text style={styles.txtTitle}>Welcome to Indica Technology</Text>
            </View>
        </>
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtTitle: {
        fontSize: font_size.VERY_LARGE_x3,
    },
});